package mc.malusartifex.pulse;

import java.io.PrintStream;
import java.io.File;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
  @Override
  public void onEnable()
  {
    //saveDefaultConfig();
    if (!new File(getDataFolder(), "config.yml").exists()) {
      saveResource("config.yml", false);
    }

    System.out.println("[Monstercraft-Tekkit Basic] Mode: Enabled");
    //System.out.println("[Monstercraft-Tekkit Basic] Unloading files...");
  }

  @Override
  public void onDisable()
  {
    System.out.println("[Monstercraft-Tekkit Basic] Mode: Disabled");
  }

  public boolean onCommand(CommandSender sender, Command command, String Label, String[] Args)
  {
    if ((sender instanceof Player)) {
      Player p = (Player)sender;

      if ((Label.equalsIgnoreCase("shop")) || (Label.equalsIgnoreCase("shops")) || (Label.equalsIgnoreCase("market"))) {
        p.chat("/warp shop");
      }

      if ((Label.equalsIgnoreCase("register")) && 
        (p.hasPermission("Monstercraft.register")) && 
        (Args.length == 0)) {
        for(Object o : getConfig().getList("register"))
        {
          if(o instanceof String)
          {
            String s = (String)o;
            p.sendMessage(s.replace("&","§"));
          }
        }
      }

      if ((Label.equalsIgnoreCase("Donate")) && 
        (Args.length == 0)) {
        for(Object o : getConfig().getList("donate"))
        {
          if(o instanceof String)
          {
            String s = (String)o;
            p.sendMessage(s.replace("&","§"));
          }
        }
      }

      if ((Label.equalsIgnoreCase("Gold")) && 
        (Args.length == 0)) {
        for(Object o : getConfig().getList("gold"))
        {
          if(o instanceof String)
          {
            String s = (String)o;
            p.sendMessage(s.replace("&","§"));
          }
        }
      }

      if ((Label.equalsIgnoreCase("Diamond")) && 
        (Args.length == 0)) {
        for(Object o : getConfig().getList("diamond"))
        {
          if(o instanceof String)
          {
            String s = (String)o;
            p.sendMessage(s.replace("&","§"));
          }
        }
      }

      if ((Label.equalsIgnoreCase("VIP")) && 
        (Args.length == 0)) {
        for(Object o : getConfig().getList("vip"))
        {
          if(o instanceof String)
          {
            String s = (String)o;
            p.sendMessage(s.replace("&","§"));
          }
        }
      }

      if (((Label.equalsIgnoreCase("VIPplus")) || (Label.equalsIgnoreCase("VIP+"))) && 
        (Args.length == 0)) {
        for(Object o : getConfig().getList("vipplus"))
        {
          if(o instanceof String)
          {
            String s = (String)o;
            p.sendMessage(s.replace("&","§"));
          }
        }
      }

      if (((Label.equalsIgnoreCase("banned")) || (Label.equalsIgnoreCase("banneditems"))) && 
        (Args.length == 0)) {
        for(Object o : getConfig().getList("banned"))
        {
          if(o instanceof String)
          {
            String s = (String)o;
            p.sendMessage(s.replace("&","§"));
          }
        }
      }

    }

    return false;
  }
}

